-- 1
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5
SELECT customerName FROM customers WHERE state IS NULL;

-- 6
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson", firstName = "Steve";

-- 7
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8
SELECT customerNumber FROM orders WHERE comments LIKE '%DHL%';

-- 9
SELECT productLine FROM productlines WHERE textDescription LIKE '%state_of_the_art%';

-- 10
SELECT DISTINCT country FROM customers;

-- 11
SELECT DISTINCT status FROM orders;

-- 12
SELECT customerName, country FROM customers WHERE country IN ('USA','FRANCE', 'CANADA');

-- 13
SELECT firstName, lastName, city FROM employees 
    JOIN offices ON employees.officeCode = offices.officeCode 
    WHERE city = "Tokyo";

-- 14
SELECT customerName, country FROM customers 
    JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber 
    WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

-- 15
SELECT products.productName, customers.customerName FROM products 
    JOIN orderdetails  ON products.productCode = orderdetails.productCode
    JOIN orders ON orders.orderNumber = orderdetails.orderNumber  
    JOIN customers ON customers.customerNumber = orders.customerNumber
    WHERE customers.customerName = "Baane Mini Imports";

-- 16
SELECT firstName, lastName, customerName, offices.country FROM employees
    JOIN offices ON offices.officeCode = employees.officeCode
    JOIN customers ON customers.country = offices.country
    WHERE customers.country = offices.country;

-- 17
SELECT productName, quantityInStock FROM products 
    JOIN productlines ON productlines.productLine = products.productLine
    WHERE quantityInStock < 1000;

-- 18
SELECT customerName FROM customers WHERE phone LIKE '%+81%';